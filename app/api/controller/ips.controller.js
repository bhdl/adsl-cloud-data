'use strict'

import Ips from '../model/ips.model'
import IpsRoutesData from '../../data/api/get.ips.route'

module.exports = (req, res) => {
  let payload = {
    gatewayId: req.params.gatewayId,
    tagId: req.query.tagId,
    start: req.query.start,
    end: req.query.end
  }

  const { error } = IpsRoutesData.validate(req.query)

  let now = Date.now()

  const timeFrameLimitInMs = 10800000

  if (!payload.start && !payload.end) {
    payload.end = now
    payload.start = payload.end - timeFrameLimitInMs
  } else if (!payload.start && payload.end) {
    payload.start = payload.end - timeFrameLimitInMs
  } else if (payload.start && !payload.end) {
    payload.end = payload.start + timeFrameLimitInMs
    if (timeFrameLimitInMs > now) {
      payload.end = now
    }
  }

  if (payload.end < payload.start) {
    let temp = payload.end
    payload.end = payload.start
    payload.start = temp
  }

  if (error) {
    return res.send({
      status: 400,
      code: 'ERROR',
      message: error.details[0].message
    })
  }

  if (payload.end - payload.start > timeFrameLimitInMs) {
    return res.send({
      status: 400,
      code: 'ERROR',
      message: 'The interval cannot exceed 3 hours'
    })
  }

  Ips.routes(payload, routes => {
    res.send({
      status: 200,
      code: 'SUCCESS',
      routes: 'Positions successfully queried',
      data: {
        positions: routes
      }
    })
  })
}
