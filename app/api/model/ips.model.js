'use strict'

import ipsStorage from '../../storages/ips'

module.exports = {
  routes: function (data, cb) {
    ipsStorage.getIpsRoutes(data).then(routes => {
      cb(routes)
    })
  }
}
