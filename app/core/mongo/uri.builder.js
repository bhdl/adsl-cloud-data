// @flow

'use strict'

function getCredentials (dbConfig: Object): string {
  if (!dbConfig.username || !dbConfig.password) {
    return ''
  }

  return `${dbConfig.username}:${dbConfig.password}@`
}

function getOptionsQueryString (options: Object): string {
  const pairs = []

  for (const [key, value] of Object.entries(options)) {
    const queryKey = encodeURIComponent(String(key))
    const queryValue = encodeURIComponent(String(value))
    pairs.push(`${queryKey}=${queryValue}`)
  }

  const queryString = pairs.join('&')
  return queryString.length > 0 ? `?${queryString}` : ''
}

export default function (dbConfig: Object): string {
  const defaults = {
    host: 'localhost'
  }

  dbConfig = Object.assign({}, defaults, dbConfig)

  // Schema
  let uri = 'mongodb://'

  // Credentials
  const credentials = getCredentials(dbConfig)
  if (credentials) {
    uri += credentials
  }

  // Host
  uri += dbConfig.host

  // Port
  if (dbConfig.port) {
    uri += `:${dbConfig.port}`
  }

  // Database & Options
  if (dbConfig.database || dbConfig.options) {
    uri += '/'
  }

  if (dbConfig.database) {
    uri += dbConfig.database
  }

  if (dbConfig.options) {
    uri += getOptionsQueryString(dbConfig.options)
  }

  return uri
}
