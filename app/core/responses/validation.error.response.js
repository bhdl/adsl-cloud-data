// @flow

'use strict'

import GeneralErrorResponse from './general.error.response'

class ValidationErrorResponse extends GeneralErrorResponse {
  constructor (packageId: string, data: Object) {
    super(400, 'The sended data is invalid', packageId, data)
  }
}

export default ValidationErrorResponse
