// @flow

'use strict'

import GeneralErrorResponse from './general.error.response'

class MongoErrorResponse extends GeneralErrorResponse {
  constructor (packageId: string, data: Object) {
    super(503, 'The sended data was not saved', packageId, data)
  }
}

export default MongoErrorResponse
