// @flow

'use strict'

import GeneralResponse from './general.response'

class GeneralSuccessResponse extends GeneralResponse {
  constructor (code: number, message: string, packageId: string, data?: Object) {
    super('OK', code, message, packageId, data)
  }
}

export default GeneralSuccessResponse
