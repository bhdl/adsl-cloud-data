// @flow

'use strict'

import GeneralResponse from './general.response'

class GeneralErrorResponse extends GeneralResponse {
  constructor (code: number, message: string, packageId: string, data?: Object) {
    super('ERROR', code, message, packageId, data)
  }
}

export default GeneralErrorResponse
