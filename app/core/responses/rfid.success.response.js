// @flow

'use strict'

import GeneralSuccessResponse from './general.success.response'

class RfidSuccessResponse extends GeneralSuccessResponse {
  constructor (packageId: string, data: Object) {
    super(200, 'The sended data is proccessed', packageId, data)
  }
}

export default RfidSuccessResponse
