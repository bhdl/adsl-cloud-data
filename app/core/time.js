// @flow
'use strict'

import utils from './utils'

import deepCopy from './deepcopy'

type RecievedDatatype = {
  timestamp: number,
  clientId: string,
  type: string,
  packageId: string,
  data: Object
}

class Time {
  currentTimeInMillis (): number {
    return new Date().getTime()
  }

  syncronizeTimestamp (dataObj: RecievedDatatype, timestampOffset: number): Object {
    if (isNaN(timestampOffset)) {
      throw new Error('Timestamp offset is not a valid number!')
    }

    if (dataObj.timestamp === Infinity) {
      throw new Error('Invalid timestamp!')
    }

    const newDataObj = deepCopy(dataObj)

    return utils.traverse(newDataObj, timestampOffset)
  }
}

export default new Time()
