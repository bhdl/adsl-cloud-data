'use strict'

import debug from 'debug'

const dev = debug('app:dev')
const info = debug('app:info')
const warn = debug('app:warn')
const error = debug('app:error')

export default { dev, info, warn, error }
