// @flow

'use strict'

import utils from './utils'

class PackageId {
  _value: string

  constructor (clientId: string, dateTime: Date) {
    this.value = {clientId: clientId, date: dateTime}
  }

  set value (value: {clientId: string, date: Date}): void {
    const clientId = value.clientId
    const date = value.date
    const year = date.getFullYear()
    const month = utils.padding(date.getMonth() + 1, 2)
    const day = utils.padding(date.getDate(), 2)
    const hours = utils.padding(date.getHours(), 2)
    const minutes = utils.padding(date.getMinutes(), 2)
    const seconds = utils.padding(date.getSeconds(), 2)
    const milliseconds = utils.padding(date.getMilliseconds(), 3)

    this._value = `${clientId}_${year}_${month}_${day}_${hours}_${minutes}_${seconds}_${milliseconds}`
  }

  get value (): string {
    return this._value
  }
}

export default PackageId
