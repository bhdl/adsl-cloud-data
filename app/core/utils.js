// @flow
'use strict'

class Utils {
  padding (number: number, width: number, character: string = '0') {
    width -= number.toString().length
    if (width > 0) {
      return new Array(width + (/\./.test(number.toString()) ? 2 : 1)).join(character) + number
    }
    return number + ''
  }

  traverse (expectedDataObj: Object, timestampOffset: number): Object {
    if (Array.isArray(expectedDataObj)) {
      this.traverseArray(expectedDataObj, timestampOffset)
    } else if ((typeof expectedDataObj === 'object') && (expectedDataObj !== null)) {
      this.traverseObject(expectedDataObj, timestampOffset)
    }
    return expectedDataObj
  }

  traverseArray (arr: Array<Object>, timestampOffset: number) {
    const _this = this
    arr.forEach(function (item) {
      _this.traverse(item, timestampOffset)
    })
  }

  traverseObject (obj: Object, timestampOffset: number) {
    for (var key in obj) {
      if (key === 'timestamp') {
        obj[key] += timestampOffset
      }
      if (obj.hasOwnProperty(key)) {
        this.traverse(obj[key], timestampOffset)
      }
    }
  }
}

export default new Utils()
