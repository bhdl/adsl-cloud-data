'use strict'

function deepCopy (obj) {
  let output, v, key
  output = Array.isArray(obj) ? [] : {}
  for (key in obj) {
    v = obj[key]
    output[key] = (typeof v === 'object') ? deepCopy(v) : v
  }
  return output
}

export default deepCopy
