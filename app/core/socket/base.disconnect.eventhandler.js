// @flow

'use strict'

import debug from '../loggers/debug'
import BaseEventHandler from './base.eventhandler'

function baseDisconnectEventHandler (socket: Object, namespace?: Object) {
  return (reason) => {
    debug.dev(`Example message. ${reason}`)
  }
}

export default new BaseEventHandler('disconnect', baseDisconnectEventHandler)
