// @flow

'use strict'

import debug from '../loggers/debug'
import Raven from '../loggers/sentry'
import BaseEventHandler from './base.eventhandler'

function baseErrorEventHandler (socket: Object, namespace?: Object) {
  return (error) => {
    Raven.captureException(error, (sendErr, eventId) => {
      if (sendErr) {
        debug.error('Failed to send captured socket exception to Sentry')
      } else {
        debug.warn('Captured socket exception and send to Sentry successfully')
      }
    })

    debug.dev(`Socket error: ${error.message}`)
  }
}

export default new BaseEventHandler('error', baseErrorEventHandler)
