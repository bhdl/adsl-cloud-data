// @flow

'use strict'

export type eventHandlerType = (socket: Object, namespace?: Object) => (data: any) => void
export type initedEventHandlerType = (data: any) => void

export default class BaseEventHandler {
  _eventName: string
  handlerMethod: eventHandlerType

  constructor (eventName: string, eventHandlerMethod: eventHandlerType) {
    this.eventName = eventName
    this.handlerMethod = eventHandlerMethod
  }

  set eventName (eventName: string): void {
    const value: string = eventName.trim().toLocaleLowerCase()

    if (value.length < 3) {
      throw new Error('Event name must be at least 3 characters')
    }

    this._eventName = value
  }

  get eventName (): string {
    return this._eventName
  }

  initEventHandler (socket: Object, namespace: Object): initedEventHandlerType {
    return this.handlerMethod(socket, namespace)
  }
}
