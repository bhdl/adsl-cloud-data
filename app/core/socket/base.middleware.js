// @flow

'use strict'

export type middlewareType = (socket: Object, next: any) => void
