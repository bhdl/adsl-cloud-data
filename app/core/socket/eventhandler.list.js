// @flow

'use strict'

import BaseEventHandler from './base.eventhandler'

export default class EventHandlerList {
  _eventHandlers: Map<string, BaseEventHandler>

  constructor (eventHandlers?: Array<BaseEventHandler>) {
    this._eventHandlers = new Map()

    const handlers = eventHandlers || []

    for (const eventHandler of handlers) {
      this.addEventHandler(eventHandler)
    }
  }

  addEventHandler (eventHandler: BaseEventHandler): void {
    const eventName: string = eventHandler.eventName

    if (this.hasEventHandler(eventName)) {
      throw new Error(`An event handler has been already registered with the given name ${eventName}`)
    }

    this._eventHandlers.set(eventName, eventHandler)
  }

  deleteEventHandler (eventName: string): boolean {
    return this._eventHandlers.delete(eventName)
  }

  hasEventHandler (eventHandlerName: string): boolean {
    return this._eventHandlers.has(eventHandlerName)
  }

  // $FlowFixMe
  [Symbol.iterator] (): Iterator<any> {
    return this._eventHandlers[Symbol.iterator]()
  }

  /*::
  @@iterator(): Iterator<any> {
    // $FlowFixMe
    return this[Symbol.iterator]()
  }
  */
}
