'use strict'

import ConfigtypeEnum from './configtype.enum'

function getRawConfig (configName) {
  const configValue = process.env[configName]

  if (!configValue) {
    throw new Error(`The '${configName}' is missing!`)
  }

  return configValue
}

function getStringConfig (configName) {
  return String(getRawConfig(configName))
}

function getIntConfig (configName) {
  const rawConfigValue = getRawConfig(configName)
  const configValue = parseInt(rawConfigValue)

  if (isNaN(configValue)) {
    throw new Error(`The ${configName} value ${rawConfigValue} is not a number!`)
  }

  return configValue
}

function getFloatConfig (configName) {
  const rawConfigValue = getRawConfig(configName)
  const configValue = parseFloat(rawConfigValue)

  if (isNaN(configValue)) {
    throw new Error(`The ${configName} value ${rawConfigValue} is not a number!`)
  }

  return configValue
}

export default function getConfig (configName, configtypeEnum = ConfigtypeEnum.STR) {
  if (configtypeEnum === ConfigtypeEnum.STR) {
    return getStringConfig(configName)
  } else if (configtypeEnum === ConfigtypeEnum.INT) {
    return getIntConfig(configName)
  } else if (configtypeEnum === ConfigtypeEnum.FLOAT) {
    return getFloatConfig(configName)
  } else {
    throw new Error(`The given type '${configtypeEnum}' is not valid!`)
  }
}
