'use strict'

import 'babel-polyfill'

import config from './config'
import createExpress from './express'
import createServer from './server'
import createSocket from './socket'
import debug from './core/loggers/debug'
import db from './db'

if (config.nodeEnv === 'PROD') {
  process.env.DEBUG = 'app:info'
}

const app = createExpress()
const server = createServer(app)
createSocket(server)

db.getConnection().catch((err) => debug.error(err))

server.listen(config.port, () => {
  debug.info(`Server listening on http://localhost:${config.port}...`)
})
