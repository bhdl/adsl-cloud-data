// @flow

'use strict'

import BaseController from '../../core/socket/base.controller'

import message from './eventhandlers/message'
import disconnect from './eventhandlers/disconnect'
import setrealtimefilter from './eventhandlers/setrealtimefilter'

import debug from '../../core/loggers/debug'

const route = /^\/gateways\/[a-z0-9]+(?:-[a-z0-9]+)*\/ips$/

const middlewares = []

const eventHandlers = [message, setrealtimefilter, disconnect];

const controller = new BaseController(route, middlewares, eventHandlers)

let clients = new Map()

let filterIsEmpty = socket => {
  return !socket.filter || !socket.filter.tagIds || !socket.filter.tagIds.length
}

let filterDataByTagId = (socket, data) => {
  return data.positions.filter(position => {
    return ~socket.filter.tagIds.indexOf(+position.tagId)
  })
}

controller.addMapClientSocket = socket => {
  if (!clients.has(socket.id)) {
    debug.info(`Socket client added to map (${socket.id})`)
    clients.set(socket.id, socket)
  }
}

controller.deleteMapClientSocket = socketId => {
  clients.delete(socketId)
}

controller.sendMapData = mapData => {
  clients.forEach(socket => {
    debug.info('forEach saved clients, current socket: ', socket.id)
    if (!filterIsEmpty(socket)) {
      mapData.data.positions = filterDataByTagId(socket, mapData.data)
    }

    socket.emit('mapdata', mapData)
    debug.info('mapData', mapData)
    debug.info('Map data sent to Web')
  })
}

export default controller
