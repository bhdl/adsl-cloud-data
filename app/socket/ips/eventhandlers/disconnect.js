'use strict'

import BaseEventHandler from '../../../core/socket/base.eventhandler'
import controller from '../controller'

function eventHandler (socket) {
  return () => {
    controller.deleteMapClientSocket(socket.id)
  }
}

export default new BaseEventHandler('disconnect', eventHandler)
