'use strict'

import BaseEventHandler from '../../../core/socket/base.eventhandler'
import time from '../../../core/time'
import validator from '../../../data/ips'
import { transformData } from '../../../models/ips'
import { transformDataToMap } from '../../../models/map'
import SuccessResponse from '../../../core/responses/ips.success.response'
import MongoErrorResponse from '../../../core/responses/mongo.error.response'
import storage from '../../../storages/ips'
import controller from '../controller'
import logger from '../../../core/loggers/logger'
import debug from '../../../core/loggers/debug'

function eventHandler (socket) {
  return data => {
    const currentTimestamp = time.currentTimeInMillis()
    const timestampOffset = currentTimestamp - data.timestamp
    logger.info('Incoming IPS message from Gateway', JSON.stringify(data))
    debug.info('Incoming IPS message from Gateway, on socket.id: ', socket.id)
    const errorResponse = validator.validate(data)

    if (errorResponse) {
      debug.error('IPS message validation failed', errorResponse.getResponseData().data)
      logger.error('IPS message validation failed', errorResponse.getResponseData().data)

      socket.emit('response', errorResponse.getResponseData())
      return
    }

    const syncronizedData = time.syncronizeTimestamp(data, timestampOffset)
    const documentList = transformData(syncronizedData)

    const mapData = transformDataToMap(syncronizedData)
    controller.sendMapData(mapData)

    storage.insertMany(documentList)
      .then(() => {
        socket.emit('response', new SuccessResponse(data.packageId, data).getResponseData())
        logger.info('Cloud has stored the message in Mongo', data)
        debug.info('Cloud has stored the message in Mongo')
      })
      .catch((err) => {
        socket.emit('response', new MongoErrorResponse(data.packageId, err).getResponseData())
        logger.error('Cloud has failed to store the message in Mongo', data)
        debug.error('Cloud has failed to store the message in Mongo')
      })
  }
}

export default new BaseEventHandler('message', eventHandler)
