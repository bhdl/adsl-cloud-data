'use strict'

import BaseEventHandler from '../../../core/socket/base.eventhandler'
import controller from '../controller'
import debug from '../../../core/loggers/debug'

function setFilterEventHandler (socket) {
  return data => {
    controller.addMapClientSocket(socket)
    socket.on('disconnect', () => {
      debug.info('Web disconnected from Cloud, socket data deleted')
      controller.deleteMapClientSocket(socket.id)
    })

    if (data.filter) {
      socket.filter = data.filter
      debug.info(`Map filter stored for client socket (${socket.id})`, socket.filter)
    }
  }
}

export default new BaseEventHandler('setrealtimefilter', setFilterEventHandler)
