// @flow

'use strict'

import BaseEventHandler from '../../../core/socket/base.eventhandler'
import time from '../../../core/time'
import validator from '../../../data/rfid'
import { transformData } from '../../../models/rfid'
import SuccessResponse from '../../../core/responses/rfid.success.response'
import MongoErrorResponse from '../../../core/responses/mongo.error.response'
import storage from '../../../storages/rfid'
import logger from '../../../core/loggers/logger'
import debug from '../../../core/loggers/debug'

function eventHandler (socket: Object, namespace?: Object) {
  return (data) => {
    const currentTimestamp = time.currentTimeInMillis()
    const timestampOffset = currentTimestamp - data.timestamp
    logger.info('Incoming RFID message from Gateway', JSON.stringify(data))
    debug.info('Cloud: Incoming RFID message from Gateway')
    const errorResponse = validator.validate(data)

    if (errorResponse) {
      socket.emit('response', errorResponse.getResponseData())
      return
    }

    const syncronizedData = time.syncronizeTimestamp(data, timestampOffset)
    const documentList = transformData(syncronizedData)

    storage.insertMany(documentList)
      .then(() => { socket.emit('response', new SuccessResponse(data.packageId, data).getResponseData()) })
      .catch((err) => { socket.emit('response', new MongoErrorResponse(data.packageId, err).getResponseData()) })
  }
}

export default new BaseEventHandler('message', eventHandler)
