// @flow

'use strict'

import BaseController from '../../core/socket/base.controller'
import message from './eventhandlers/message'

const route = /^\/gateways\/[a-z0-9]+(?:-[a-z0-9]+)*\/rfid$/

const middlewares = []

const eventHandlers = [message]

export default new BaseController(route, middlewares, eventHandlers)
