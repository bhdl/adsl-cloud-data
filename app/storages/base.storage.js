'use strict'

import db from '../db'
import Raven from '../core/loggers/sentry'

export default class BaseStorage {
  constructor (collectionName) {
    this._collection = collectionName
    this._db = db
  }

  async insertMany (documents) {
    return new Promise((resolve, reject) => {
      this._db.getConnection()
        .then((connection) => { this._insertMany(connection, documents) })
        .then((result) => { resolve(result) })
        .catch((err) => { reject(err) })
    })
  }

  _insertMany (connection, documents) {
    return new Promise((resolve, reject) => {
      connection.collection(this._collection).insertMany(documents, (err, result) => {
        if (err) {
          Raven.captureException(err)
          return reject(err)
        }

        return resolve(result)
      })
    })
  }

  async getIpsRoutes (data) {
    return new Promise((resolve, reject) => {
      this._db.getConnection()
        .then(connection => { return this._getIpsRoutes(connection, data) })
        .then(result => { resolve(result) })
        .catch(err => { reject(err) })
    })
  }

  _getIpsRoutes (connection, data) {
    return new Promise((resolve, reject) => {
      connection.collection(this._collection)
        .find({
          gatewayId: data.gatewayId,
          tagId: data.tagId,
          timestamp: { $gte: +data.start, $lte: +data.end }
        })
        .project({ geolocation: 1, _id: 0 })
        .toArray((error, result) => {
          if (error) {
            Raven.captureException(error)
            return reject(error)
          }

          for (var i in result) {
            result[i].latitude = result[i].geolocation.latitude
            result[i].longitude = result[i].geolocation.longitude
            delete result[i].geolocation
          }

          resolve(result)
        })
    })
  }
}
