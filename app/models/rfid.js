'use strict'

function transformData (rfidDataPackage) {
  let documentList = []

  let commonData = {
    packageId: rfidDataPackage.packageId,
    gatewayId: rfidDataPackage.gatewayId,
    clientId: rfidDataPackage.clientId,
    readerTemperature: rfidDataPackage.data.readerStatus.temperature
  }

  for (const rfidData of rfidDataPackage.data.rfData) {
    let document = {...commonData}

    document.timestamp = rfidData.timestamp
    document.tagId = rfidData.tagId
    document.antennaId = rfidData.antennaId
    document.rssi = rfidData.rssi

    documentList.push(document)
  }

  return documentList
}

export { transformData }
