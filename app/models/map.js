'use strict'

import PackageId from '../core/packageid'
import config from '../config'

function transformDataToMap (ipsDataPackage) {
  const date = new Date()

  const transformedData = {
    timestamp: date.getTime(),
    gatewayId: ipsDataPackage.gatewayId,
    clientId: config.appName,
    type: 'map_data',
    packageId: new PackageId(config.appName, date).value,
    data: {
      positions: []
    }
  }

  for (const positionData of ipsDataPackage.data.positions) {
    const transformedPosition = {
      tagId: positionData.tagId,

      geolocation: {
        latitude: positionData.geolocation.latitude,
        longitude: positionData.geolocation.longitude,
        accuracy: positionData.relativeLocation.accuracy
      }
    }

    transformedData.data.positions.push(transformedPosition)
  }

  return transformedData
}

export { transformDataToMap }
