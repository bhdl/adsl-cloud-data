'use strict'

function transformData (ipsDataPackage) {
  let documentList = []

  let commonData = {
    packageId: ipsDataPackage.packageId,
    gatewayId: ipsDataPackage.gatewayId,
    clientId: ipsDataPackage.clientId
  }

  for (const positionData of ipsDataPackage.data.positions) {
    let document = {...commonData}

    document.tagId = positionData.tagId
    document.timestamp = positionData.timestamp
    document.relativeLocation = {
      x: positionData.relativeLocation.x,
      y: positionData.relativeLocation.y,
      z: positionData.relativeLocation.z,
      accuracy: positionData.relativeLocation.accuracy
    }

    document.geolocation = {
      latitude: positionData.geolocation.latitude,
      longitude: positionData.geolocation.longitude
    }

    documentList.push(document)
  }

  return documentList
}

export { transformData }
