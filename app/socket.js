'use strict'

import ioServer from 'socket.io'

import redis from 'socket.io-redis'
import BaseController from './core/socket/base.controller'
import ipsController from './socket/ips/controller'
import rfidController from './socket/rfid/controller'
import { redisConfig } from './config'

function createSocket (server) {
  const io = ioServer(server)

  io.adapter(redis({host: redisConfig.host, port: redisConfig.port}))
  BaseController.initController(io, ipsController)
  BaseController.initController(io, rfidController)
  return io
}

export default createSocket
