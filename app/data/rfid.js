'use strict'

import Joi from 'joi'

import commonSchema from './common'
import Validator from '../core/models/validator'

const readerStatusSchema = Joi.object({
  temperature: Joi.number().required()
})

const rfDataSchema = Joi.object({
  tagId: Joi.string().required(),
  antennaId: Joi.string().required(),
  rssi: Joi.number(),
  timestamp: Joi.date().timestamp('javascript').required()
})

const dataPackageSchema = Joi.object({
  readerStatus: readerStatusSchema,
  rfData: Joi.array().min(1).items(rfDataSchema).required()
})

const rfidSchema = Joi.object({
  ...commonSchema,
  data: dataPackageSchema
})

export default new Validator(rfidSchema)
