'use strict'

import Joi from 'joi'

const routeSchema = Joi.object({
  tagId: Joi.string().required(),
  start: Joi.date(),
  end: Joi.date()
})

module.exports = {
  validate: function (data) {
    return Joi.validate(data, routeSchema)
  }
}
