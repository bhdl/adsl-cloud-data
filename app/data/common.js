'use strict'

import Joi from 'joi'

import { formatConfig } from '../config'

const commonSchema = {
  timestamp: Joi.number().positive().required(),
  gatewayId: Joi.string().required(),
  clientId: Joi.string().required(),
  type: Joi.string().allow('rfid', 'ips'),
  packageId: Joi.string().regex(formatConfig.packageId)
}

export default commonSchema
