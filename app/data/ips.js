'use strict'

import Joi from 'joi'

import commonSchema from './common'
import Validator from '../core/models/validator'

const relativeLocationSchema = Joi.object({
  x: Joi.number().required(),
  y: Joi.number().required(),
  z: Joi.number().required(),
  accuracy: Joi.number().required()
})

const geoLocationSchema = Joi.object({
  latitude: Joi.number().required(),
  longitude: Joi.number().required()
})

const positionSchema = Joi.object({
  tagId: Joi.string().required(),
  timestamp: Joi.date().timestamp('javascript').required(),
  relativeLocation: relativeLocationSchema,
  geolocation: geoLocationSchema
})

const dataPackageSchema = Joi.object({
  positions: Joi.array().min(1).items(positionSchema).required()
})

const ipsSchema = Joi.object({
  ...commonSchema,
  data: dataPackageSchema
})

export default new Validator(ipsSchema)
