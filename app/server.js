'use strict'

import { Server } from 'http'

function createServer (expressApp) {
  return Server(expressApp)
}

export default createServer
