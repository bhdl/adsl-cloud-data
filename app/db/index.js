import { dbConfig } from '../config'
import DB from '../core/mongo/db'
import uriBuilder from '../core/mongo/uri.builder'

// Connection String
const url = uriBuilder(dbConfig)

export default new DB(url, dbConfig.database)
