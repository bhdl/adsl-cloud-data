'use strict'

import express from 'express'
import bodyParser from 'body-parser'

import Raven from './core/loggers/sentry'

import ipsApiController from './api/controller/ips.controller'

let router = express.Router()
function createExpress () {
  const app = express()

  app.use(Raven.requestHandler())
  app.use(Raven.errorHandler())

  app.use(bodyParser.urlencoded({ extended: true }))
  app.use(bodyParser.json())

  router.route('/gateways/:gatewayId/route').get(ipsApiController)

  app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    next()
  })

  app.use('/api/:version', router)

  return app
}

export default createExpress
