# ADSL Cloud Data Service

Arteries Drone System League - Cloud Data Service webapp.

## Installation

This is a [Node.js](https://nodejs.org/en/) and
[mongoDB](https://www.mongodb.com/) based webapplication.

### Prerequisites

Before installing,

- [download and install Node.js](https://nodejs.org/en/download/),
- [install mongoDB](https://www.mongodb.com/)
- [install redis](https://redis.io/)

### Installation steps

Clone the project from [Gitlab](https://gitlab.com/bhdl/bhdl-cloud-data-refactor)

```bash
$ git clone <repository> [<directory>]
```

Enter to the project root directory

```bash
$ cd [<directory>]
```

Dependencies installation is done using the
[npm install command](https://docs.npmjs.com/getting-started/installing-npm-packages-locally):

```bash
$ npm install
```

Copy and setup the required configs

```bash
$ cp .env.example .env
$ <editor> .env
```

## Npm Scripts

Here some usefor npm script description. For all scripts please visit the
`./package.json` at the `scripts` section

### Running Dev Server

```bash
$ npm run dev
```

### Running Unit Tests

```bash
$ npm run test
```

### Running Linter

```bash
$ npm run lint
```

### Running Build Process

```bash
$ npm run build
```

### Running Prod Sever

```bash
$ npm run prod
```

## Running solution with pm2

### Install pm2 globally

```bash
$ npm i -g pm2
```

### Run Build Script

```bash
$ npm run build
```

### Start Process with PM2

```bash
$ pm2 start ./dist/index.js -i 0
```
