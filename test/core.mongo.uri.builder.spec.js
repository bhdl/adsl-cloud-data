'use strict'

import { expect } from 'chai'

import uriBuilder from '../app/core/mongo/uri.builder'

describe('Uri bulder', function () {
  const testDbConfigs = [
    {
      name: '"{}"',
      dbConfig: {},
      expected: 'mongodb://localhost'
    },
    {
      name: '"{host: adsl.arteries.hu}"',
      dbConfig: {
        host: 'adsl.arteries.hu'
      },
      expected: 'mongodb://adsl.arteries.hu'
    },
    {
      name: '"{username: apple}"',
      dbConfig: {
        username: 'apple'
      },
      expected: 'mongodb://localhost'
    },
    {
      name: '"{password: apple}"',
      dbConfig: {
        password: 'apple'
      },
      expected: 'mongodb://localhost'
    },
    {
      name: '"{username: apple, password: pwd}"',
      dbConfig: {
        username: 'apple',
        password: 'pwd'
      },
      expected: 'mongodb://apple:pwd@localhost'
    },
    {
      name: '"{host: adsl.arteries.hu, username: apple, password: pwd}"',
      dbConfig: {
        host: 'adsl.arteries.hu',
        username: 'apple',
        password: 'pwd'
      },
      expected: 'mongodb://apple:pwd@adsl.arteries.hu'
    },
    {
      name: '"{host: adsl.arteries.hu, username: apple, password: pwd, port: 6587}"',
      dbConfig: {
        host: 'adsl.arteries.hu',
        username: 'apple',
        password: 'pwd',
        port: '6587'
      },
      expected: 'mongodb://apple:pwd@adsl.arteries.hu:6587'
    },
    {
      name: '"{host: adsl.arteries.hu, username: apple, password: pwd, port: 6587, database: adsl}"',
      dbConfig: {
        host: 'adsl.arteries.hu',
        username: 'apple',
        password: 'pwd',
        port: '6587',
        database: 'adsl'
      },
      expected: 'mongodb://apple:pwd@adsl.arteries.hu:6587/adsl'
    },
    {
      name: '"{host: adsl.arteries.hu, username: apple, password: pwd, port: 6587, database: adsl, options: {a: b}}"',
      dbConfig: {
        host: 'adsl.arteries.hu',
        username: 'apple',
        password: 'pwd',
        port: '6587',
        database: 'adsl',
        options: {
          a: 'b'
        }
      },
      expected: 'mongodb://apple:pwd@adsl.arteries.hu:6587/adsl?a=b'
    },
    {
      name: '"{host: adsl.arteries.hu, username: apple, password: pwd, port: 6587, database: adsl, options: {a: b, d: e, f: g}}"',
      dbConfig: {
        host: 'adsl.arteries.hu',
        username: 'apple',
        password: 'pwd',
        port: '6587',
        database: 'adsl',
        options: {
          a: 'b',
          d: 'e',
          f: 'g'
        }
      },
      expected: 'mongodb://apple:pwd@adsl.arteries.hu:6587/adsl?a=b&d=e&f=g'
    },
    {
      name: '"{host: adsl.arteries.hu, username: apple, password: pwd, port: 6587, options: {a: b, d: e, f: g}}"',
      dbConfig: {
        host: 'adsl.arteries.hu',
        username: 'apple',
        password: 'pwd',
        port: '6587',
        options: {
          a: 'b',
          d: 'e',
          f: 'g'
        }
      },
      expected: 'mongodb://apple:pwd@adsl.arteries.hu:6587/?a=b&d=e&f=g'
    },
    {
      name: '"{host: adsl.arteries.hu, port: 6587, database: adsl, options: {a: b, d: e, f: g}}"',
      dbConfig: {
        host: 'adsl.arteries.hu',
        port: '6587',
        database: 'adsl',
        options: {
          a: 'b',
          d: 'e',
          f: 'g'
        }
      },
      expected: 'mongodb://adsl.arteries.hu:6587/adsl?a=b&d=e&f=g'
    },
    {
      name: '"{username: apple, password: pwd, port: 6587, database: adsl, options: {a: b, d: e, f: g}}"',
      dbConfig: {
        username: 'apple',
        password: 'pwd',
        port: '6587',
        database: 'adsl',
        options: {
          a: 'b',
          d: 'e',
          f: 'g'
        }
      },
      expected: 'mongodb://apple:pwd@localhost:6587/adsl?a=b&d=e&f=g'
    }
  ]
  testDbConfigs.forEach(function (testDbConfig) {
    it(`should return correct string when parameter is ${testDbConfig.name}`, function () {
      const uriString = uriBuilder(testDbConfig.dbConfig)
      expect(uriString).to.be.equal(testDbConfig.expected)
    })
  })
})
