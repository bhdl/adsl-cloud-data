'use strict'

import { expect } from 'chai'

import logger from '../app/core/loggers/logger'

describe('Logger', function () {
  const levels = ['debug', 'info', 'notice', 'warning', 'error']
  levels.forEach(function (level) {
    it(`should have ${level} method`, function () {
      expect(logger).to.have.property(level)
    })

    it(`should ${level} method is a function`, function () {
      expect(logger[level]).to.be.a('function')
    })
  })
})
