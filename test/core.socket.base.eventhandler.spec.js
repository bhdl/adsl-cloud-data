'use strict'

import { expect } from 'chai'
import sinon from 'sinon'

import BaseEventHandler from '../app/core/socket/base.eventhandler'

describe('BaseEventHandler', function () {
  describe('set name', function () {
    const testNames = [
      {
        testName: 'CONnect',
        expected: 'connect'
      },
      {
        testName: ' CONNECT',
        expected: 'connect'
      },
      {
        testName: ' CONNECT     ',
        expected: 'connect'
      },
      {
        testName: 'cONNECT   ',
        expected: 'connect'
      }
    ]

    testNames.forEach(function (testName) {
      it(`should trim and transform to lower case when the given value is "${testName.testName}"`, function () {
        const baseEventHandler = new BaseEventHandler(testName.testName, () => {})
        expect(baseEventHandler.eventName).to.be.equal(testName.expected)
      })
    })

    const testErrorNames = [
      {
        testName: ''
      },
      {
        testName: 'A'
      },
      {
        testName: 13
      },
      {
        testName: '  AB  '
      }
    ]

    testErrorNames.forEach(function (testName) {
      it(`should trow error when the name's lenght is less then 3 characters and the given value is "${testName.testName}"`, function () {
        function pass (arg) {
          return arg
        }

        const throwError = function () {
          const baseEventHandler = new BaseEventHandler(testName.testName, () => {})
          pass(baseEventHandler)
        }
        expect(throwError).to.throw(Error)
      })
    })
  })

  describe('initEventHandler', function () {
    let baseEventHandler
    let spy

    before(function () {
      baseEventHandler = new BaseEventHandler('test', (socket, namespace) => {
        return () => {}
      })
      spy = sinon.spy(baseEventHandler, 'handlerMethod')
    })

    it('should call the handlerMethod function', function () {
      baseEventHandler.initEventHandler()
      expect(spy.calledOnce).to.be.equal(true)
    })

    it('should call the handlerMethod function with the given parameters', function () {
      baseEventHandler.initEventHandler(12, 20)
      expect(spy.withArgs(12, 20).calledOnce).to.be.equal(true)
    })

    it('should return an initialized handlerMethod function', function () {
      baseEventHandler.initEventHandler(12, 20)
      const spyReturn = spy.returnValues[0]
      expect(spyReturn).to.be.a('function')
    })

    after(function () {
      baseEventHandler.handlerMethod.restore()
    })
  })
})
