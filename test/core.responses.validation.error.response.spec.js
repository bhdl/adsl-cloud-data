'use strict'

import { expect } from 'chai'

import ValidationErrorResponse from '../app/core/responses/validation.error.response'

describe('Validation error response', function () {
  let validationErrorResponse

  beforeEach(function () {
    validationErrorResponse = new ValidationErrorResponse('Unknow_0000_00_00_00_00_00_000', {})
  })

  describe('Constructor', function () {
    it('should set the code attribute to "400"', function () {
      expect(validationErrorResponse.code).to.be.equal(400)
    })

    it('should set the message attribute "The sended data is invalid"', function () {
      expect(validationErrorResponse.message).to.be.equal('The sended data is invalid')
    })
  })
})
