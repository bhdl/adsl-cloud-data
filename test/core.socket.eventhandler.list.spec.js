'use strict'

import { expect } from 'chai'

import BaseEventHandler from '../app/core/socket/base.eventhandler'
import EventHandlerList from '../app/core/socket/eventhandler.list'

describe('EventHandlerList', function () {
  let eventHandlerList

  beforeEach(function () {
    eventHandlerList = new EventHandlerList()
  })

  describe('has eventhandler', function () {
    it('should return true when it cotains an event handler with the given name', function () {
      const eventName = 'test'
      const baseEventHandler = new BaseEventHandler(eventName, () => {})
      eventHandlerList.addEventHandler(baseEventHandler)
      const hasEventhandler = eventHandlerList.hasEventHandler(eventName)
      expect(hasEventhandler).to.be.equal(true)
    })

    it('should return false when it does not contain any event handler with the given name', function () {
      const eventName = 'connect'
      const baseEventHandler = new BaseEventHandler(eventName, () => {})
      eventHandlerList.addEventHandler(baseEventHandler)
      const hasEventhandler = eventHandlerList.hasEventHandler('xyz')
      expect(hasEventhandler).to.be.equal(false)
    })
  })

  describe('add eventhandler', function () {
    it('should add the given eventhandler to its list', function () {
      const eventName = 'test'
      const baseEventHandler = new BaseEventHandler(eventName, () => {})
      eventHandlerList.addEventHandler(baseEventHandler)
      const hasEventhandler = eventHandlerList.hasEventHandler(eventName)
      expect(hasEventhandler).to.be.equal(true)
    })

    it('should throw an error when the eventhanderlist contains a same named eventhandler', function () {
      const eventName = 'test'
      const baseEventHandler = new BaseEventHandler(eventName, () => {})
      eventHandlerList.addEventHandler(baseEventHandler)
      const throwError = function () { eventHandlerList.addEventHandler(baseEventHandler) }
      expect(throwError).to.throw(Error)
    })
  })

  describe('delete eventhandler', function () {
    it('should delete the eventhandler which has the given name', function () {
      const eventName = 'test'
      const baseEventHandler = new BaseEventHandler(eventName, () => {})
      eventHandlerList.addEventHandler(baseEventHandler)
      eventHandlerList.deleteEventHandler(eventName)
      const hasEventhandler = eventHandlerList.hasEventHandler(eventName)
      expect(hasEventhandler).to.be.equal(false)
    })
  })

  describe('iterator', function () {
    it('could be iterated', function () {
      const iterator = (new EventHandlerList())[Symbol.iterator]
      expect(iterator).not.to.be.an('undefined')
    })
  })
})
