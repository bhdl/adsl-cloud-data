'use strict'

import { expect } from 'chai'

import IpsSuccessResponse from '../app/core/responses/ips.success.response'

describe('IPS success response', function () {
  let ipsSuccessResponse

  beforeEach(function () {
    ipsSuccessResponse = new IpsSuccessResponse('Unknow_0000_00_00_00_00_00_000', {})
  })

  describe('Constructor', function () {
    it('should set the code attribute to "200"', function () {
      expect(ipsSuccessResponse.code).to.be.equal(200)
    })

    it('should set the message attribute "The sended data is proccessed"', function () {
      expect(ipsSuccessResponse.message).to.be.equal('The sended data is proccessed')
    })
  })
})
