'use strict'

import { expect } from 'chai'

import time from '../app/core/time'

describe('Time', function () {
  describe('currentTimeInMillis', function () {
    it('shoul return the current time in UNIX time', function () {
      const currentTime = new Date().getTime()
      expect(time.currentTimeInMillis()).to.be.closeTo(currentTime, 100)
    })
  })

  describe('Syncronize timestamp', function () {
    it('should throw an error when the given timestamp offset is undefined', function () {
      const testData = {timestamp: 1535536383111}
      const throwError = () => { time.syncronizeTimestamp(testData, undefined) }
      expect(throwError).to.throw(Error)
    })

    it('should throw an error when the given timestamp offset is NaN', function () {
      const testData = {timestamp: 1535536383111}
      const throwError = () => { time.syncronizeTimestamp(testData, NaN) }
      expect(throwError).to.throw(Error)
    })

    it('should throw an error when the given timestamp is infinity', function () {
      const testData = {timestamp: Infinity}
      const throwError = () => { time.syncronizeTimestamp(testData, 0) }
      expect(throwError).to.throw(Error)
    })

    it('should apply timeoffset correctly when data type is position', function () {
      const testData = {
        timestamp: 1535459539539,
        clientId: 'ips01',
        type: 'position',
        packageId: 'ips01_2018_08_21_10_43_21_123',
        data: {
          positions: [
            {
              tagId: '1',
              timestamp: 1535459410410,
              relativeLocation: {
                x: 1.6,
                y: 12.45,
                z: 2.4,
                accurancy: 0.12
              },
              geolocation: {
                latitude: 1.23455,
                longitude: 1.23344
              }
            },
            {
              tagId: '2',
              timestamp: 1535459310310,
              relativeLocation: {
                x: 1.3,
                y: 13.5,
                z: 3.4,
                accurancy: 0.15
              },
              geolocation: {
                latitude: 1.23444,
                longitude: 1.23444
              }
            },
            {
              tagId: '3',
              timestamp: 1535459510510,
              relativeLocation: {
                x: 2.8,
                y: 20.6,
                z: 1.8,
                accurancy: 0.13
              },
              geolocation: {
                latitude: 1.23555,
                longitude: 1.23555
              }
            }
          ]
        }
      }

      const currentTime = new Date().getTime()
      const timeOffset = currentTime - testData.timestamp
      const newObj = time.syncronizeTimestamp(testData, timeOffset)

      for (let i = 0; i < testData.data.positions.length; i++) {
        const computedTime = newObj.data.positions[i].timestamp
        const expectedTime = testData.data.positions[i].timestamp + timeOffset
        expect(computedTime).to.be.equal(expectedTime)
      }
    })

    it('should apply timeoffset correctly when data type is rfid', function () {
      const testData = {
        timestamp: 1534313277452,
        clientId: 'cl01',
        type: 'rfid',
        packageId: 'cl01_2018_08_21_10_43_21_123',
        data: {
          readerStatus: {
            temperature: 48
          },
          rfData: [
            {
              tagId: 'rf01',
              antennaId: 'an02',
              rssi: 0,
              timestamp: 1534313427452
            },
            {
              tagId: 'rf02',
              antennaId: 'an02',
              rssi: 0,
              timestamp: 1534313427555
            },
            {
              tagId: 'rf03',
              antennaId: 'an05',
              rssi: 0,
              timestamp: 1534313427888
            }
          ]
        }
      }

      const currentTime = new Date().getTime()
      const timeOffset = currentTime - testData.timestamp
      const newObj = time.syncronizeTimestamp(testData, timeOffset)

      for (let i = 0; i < testData.data.rfData.length; i++) {
        const computedTime = newObj.data.rfData[i].timestamp
        const expectedTime = testData.data.rfData[i].timestamp + timeOffset
        expect(computedTime).to.be.equal(expectedTime)
      }
    })
  })
})
