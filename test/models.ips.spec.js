'use strict'

import { expect } from 'chai'

import { transformData } from '../app/models/ips'

describe('IPS model transform', () => {
  it('should be correct', () => {
    const testData = {
      timestamp: 1535459539539,
      clientId: 'ips01',
      type: 'position',
      packageId: 'ips01_2018_08_21_10_43_21_123',
      data: {
        positions: [
          {
            tagId: '1',
            timestamp: 1535459410410,
            relativeLocation: {
              x: 1.6,
              y: 12.45,
              z: 2.4,
              accurancy: 0.12
            },
            geolocation: {
              latitude: 1.23455,
              longitude: 1.23344
            }
          },
          {
            tagId: '2',
            timestamp: 1535459310310,
            relativeLocation: {
              x: 1.3,
              y: 13.5,
              z: 3.4,
              accurancy: 0.15
            },
            geolocation: {
              latitude: 1.23444,
              longitude: 1.23444
            }
          },
          {
            tagId: '3',
            timestamp: 1535459510510,
            relativeLocation: {
              x: 2.8,
              y: 20.6,
              z: 1.8,
              accurancy: 0.13
            },
            geolocation: {
              latitude: 1.23555,
              longitude: 1.23555
            }
          }
        ]
      }
    };

    const transformedData = transformData(testData);
    for (let i = 0; i < testData.data.positions.length; i++) {
      const exceptedData = testData.data.positions[i];
      expect(transformedData[i].packageId).to.be.equal(testData.packageId);
      expect(transformedData[i].clientId).to.be.equal(testData.clientId);
      expect(transformedData[i].tagId).to.be.equal(exceptedData.tagId);
      expect(transformedData[i].relativeLocation.x).to.be.equal(
        exceptedData.relativeLocation.x
      );
      expect(transformedData[i].relativeLocation.y).to.be.equal(
        exceptedData.relativeLocation.y
      );
      expect(transformedData[i].relativeLocation.z).to.be.equal(
        exceptedData.relativeLocation.z
      );
      expect(transformedData[i].relativeLocation.accuracy).to.be.equal(
        exceptedData.relativeLocation.accuracy
      );
      expect(transformedData[i].geolocation.latitude).to.be.equal(
        exceptedData.geolocation.latitude
      );
      expect(transformedData[i].geolocation.longitude).to.be.equal(
        exceptedData.geolocation.longitude
      );
    }
  });
});
