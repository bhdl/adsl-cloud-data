'use strict'

import { expect } from 'chai'

import RfidSuccessResponse from '../app/core/responses/rfid.success.response'

describe('Rfid success response', function () {
  let rfidSuccessResponse

  beforeEach(function () {
    rfidSuccessResponse = new RfidSuccessResponse('Unknow_0000_00_00_00_00_00_000', {})
  })

  describe('Constructor', function () {
    it('should set the code attribute to "200"', function () {
      expect(rfidSuccessResponse.code).to.be.equal(200)
    })

    it('should set the message attribute "The sended data is proccessed"', function () {
      expect(rfidSuccessResponse.message).to.be.equal('The sended data is proccessed')
    })
  })
})
