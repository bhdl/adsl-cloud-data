'use strict'

import { expect } from 'chai'

import { transformData } from '../app/models/rfid'

describe('RFID model transform', function () {
  it('should be correct', function () {
    const testData = {
      timestamp: 1534313277452,
      clientId: 'cl01',
      type: 'rfid',
      packageId: 'cl01_2018_08_21_10_43_21_123',
      data: {
        readerStatus: {
          temperature: 48
        },
        rfData: [
          {
            tagId: 'rf01',
            antennaId: 'an02',
            rssi: 0,
            timestamp: 1534313427452
          },
          {
            tagId: 'rf02',
            antennaId: 'an02',
            rssi: 0,
            timestamp: 1534313427555
          },
          {
            tagId: 'rf03',
            antennaId: 'an05',
            rssi: 0,
            timestamp: 1534313427888
          }
        ]
      }
    }

    const transformedData = transformData(testData)
    for (let i = 0; i < testData.data.rfData.length; i++) {
      const exceptedData = testData.data.rfData[i]
      expect(transformedData[i].timestamp).to.be.equal(exceptedData.timestamp)
      expect(transformedData[i].packageId).to.be.equal(testData.packageId)
      expect(transformedData[i].clientId).to.be.equal(testData.clientId)
      expect(transformedData[i].tagId).to.be.equal(exceptedData.tagId)
      expect(transformedData[i].antennaId).to.be.equal(exceptedData.antennaId)
      expect(transformedData[i].rssi).to.be.equal(exceptedData.rssi)
      expect(transformedData[i].readerTemperature).to.be.equal(testData.data.readerStatus.temperature)
    }
  })
})
