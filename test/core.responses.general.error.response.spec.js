'use strict'

import { expect } from 'chai'

import GeneralErrorResponse from '../app/core/responses/general.error.response'

describe('General error response', function () {
  let generalErrorResponse

  beforeEach(function () {
    generalErrorResponse = new GeneralErrorResponse(404, 'My message', 'Unknow_0000_00_00_00_00_00_000', {})
  })

  describe('Constructor', function () {
    it('should set the status attribute to "ERROR"', function () {
      expect(generalErrorResponse.status).to.be.equal('ERROR')
    })
  })
})
