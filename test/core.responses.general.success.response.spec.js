'use strict'

import { expect } from 'chai'

import GeneralSuccessResponse from '../app/core/responses/general.success.response'

describe('General success response', function () {
  let generalSuccessResponse

  beforeEach(function () {
    generalSuccessResponse = new GeneralSuccessResponse(203, 'My message', 'Unknow_0000_00_00_00_00_00_000', {})
  })

  describe('Constructor', function () {
    it('should set the status attribute to "OK"', function () {
      expect(generalSuccessResponse.status).to.be.equal('OK')
    })
  })
})
