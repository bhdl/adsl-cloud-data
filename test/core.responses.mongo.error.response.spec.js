'use strict'

import { expect } from 'chai'

import MongoErrorResponse from '../app/core/responses/mongo.error.response'

describe('Mongo error response', function () {
  let mongoErrorResponse

  beforeEach(function () {
    mongoErrorResponse = new MongoErrorResponse('Unknow_0000_00_00_00_00_00_000', {})
  })

  describe('Constructor', function () {
    it('should set the code attribute to "503"', function () {
      expect(mongoErrorResponse.code).to.be.equal(503)
    })

    it('should set the message attribute "The sended data was not saved"', function () {
      expect(mongoErrorResponse.message).to.be.equal('The sended data was not saved')
    })
  })
})
