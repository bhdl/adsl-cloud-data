'use strict'

import { expect } from 'chai'

import GeneralResponse from '../app/core/responses/general.response'

describe('General Response', function () {
  let generalResponse

  beforeEach(function () {
    generalResponse = new GeneralResponse('OK', 200, 'My message', 'Unknow_0000_00_00_00_00_00_000', {})
  })

  describe('get minStatusCode', function () {
    it('should be 100', function () {
      expect(generalResponse.minStatusCode).to.be.equal(100)
    })
  })

  describe('get maxStatusCode', function () {
    it('should be 600', function () {
      expect(generalResponse.maxStatusCode).to.be.equal(600)
    })
  })

  describe('getResponseData', function () {
    const properties = [
      'status',
      'code',
      'message',
      'packageId',
      'data'
    ]

    properties.forEach(function (property) {
      it(`should return with an object which has the '${property}' property`, function () {
        const responseData = generalResponse.getResponseData()
        expect(responseData).to.have.property(property)
      })
    })
  })

  describe('set code', function () {
    it('should throw a type error when the given value is NaN', function () {
      const throwError = () => { generalResponse.code = NaN }
      expect(throwError).to.throw(TypeError)
    })

    it('should throw a type error when the given value is infinity', function () {
      const throwError = () => { generalResponse.code = Infinity }
      expect(throwError).to.throw(TypeError)
    })

    it(`should throw an error when the given value is less then 100`, function () {
      const throwError = () => { generalResponse.code = generalResponse.minStatusCode - 1 }
      expect(throwError).to.throw(Error)
    })

    it(`should throw an error when the given value is bigger then 600`, function () {
      const throwError = () => { generalResponse.code = generalResponse.maxStatusCode }
      expect(throwError).to.throw(Error)
    })
  })

  describe('set packageId', function () {
    const invalidPackageIds = [
      12354,
      'client01',
      'a_asdf_as_ef_rt_rt_rt_erz',
      'cli01_2018_09_01',
      '2018_09_01_12_10_15_456',
      'cli01_1535379022'
    ]

    invalidPackageIds.forEach(function (packageID) {
      it(`should throw an error when the given value's format is invalid '${packageID}'`, function () {
        const throwError = () => { generalResponse.packageId = packageID }
        expect(throwError).to.throw(Error)
      })
    })

    const validPackageIds = [
      'client01_2018_09_01_11_10_22_123',
      'UNKNOWN_0000_00_00_00_00_00_000',
      'client-01_1999_12_31_15_29_54_999'
    ]

    validPackageIds.forEach(function (packageID) {
      it(`should set the given correctly formatted given value '${packageID}'`, function () {
        generalResponse.packageId = packageID
        expect(generalResponse.packageId).to.be.equal(packageID)
      })
    })
  })
})
