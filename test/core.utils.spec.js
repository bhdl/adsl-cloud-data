'use strict'

import { expect } from 'chai'

import utils from '../app/core/utils'

describe('Utils', function () {
  describe('padding', function () {
    const testData = [
      {
        number: 5,
        width: 5,
        expected: '00005'
      },
      {
        number: 555,
        width: 3,
        expected: '555'
      },
      {
        number: 18,
        width: 5,
        expected: '00018'
      }
    ]
    testData.forEach(function (testElement) {
      it(`should padding the given number (${testElement.number}) to the given width (${testElement.width}) string (${testElement.expected})`, function () {
        const paddingString = utils.padding(testElement.number, testElement.width)
        expect(paddingString).to.be.equal(testElement.expected)
      })
    })

    it('should padding with the given character ("x")', function () {
      const paddingString = utils.padding(52, 10, 'x')
      expect(paddingString).to.be.equal('xxxxxxxx52')
    })

    it('should padding when the given width less then number length', function () {
      const paddingString = utils.padding(52324272, 6, 'x')
      expect(paddingString).to.be.equal('52324272')
    })
  })

  describe('traverse', function () {
    const testData = [15, 'asdfgh', true, null, undefined, Infinity, NaN]
    testData.forEach(function (testElement) {
      it(`should be correct traverse when given simple datatype is ${testElement}`, function () {
        const traverseElement = utils.traverse(testElement, 10)
        expect(traverseElement).to.be.deep.equal(testElement)
      })
    })

    it('should be correct traverse when given array of object', function () {
      const testData = [
        { timestamp: 10, apple: 14, pear: 15, cherry: 34 },
        { window: 'aaa', book: 'bbb' },
        { a: 'a', b: 'b', timestamp: 25 }
      ]
      const exceptedData = [
        { timestamp: 20, apple: 14, pear: 15, cherry: 34 },
        { window: 'aaa', book: 'bbb' },
        { a: 'a', b: 'b', timestamp: 35 }
      ]
      const traverseData = utils.traverse(testData, 10)
      expect(traverseData).to.be.deep.equal(exceptedData)
    })

    it('should be correct traverse when given nested object', function () {
      const testData = [
        { timestamp: 10,
          apple: 14,
          pear: 15,
          cherry: 34,
          melon: {
            seed: 15,
            water: 81,
            timestamp: 34,
            peel: {
              a: 'a',
              timestamp: 21,
              b: 'b'
            }
          }
        },
        { a: 'a',
          timestamp: 56,
          b: {
            c: {
              d: {
                timestamp: 34,
                e: 23
              },
              f: 78
            },
            g: {
              h: 'rtz',
              timestamp: 45
            },
            j: {
              timestamp: 56
            }
          }
        }
      ]
      const exceptedData = [
        { timestamp: 20,
          apple: 14,
          pear: 15,
          cherry: 34,
          melon: {
            seed: 15,
            water: 81,
            timestamp: 44,
            peel: {
              a: 'a',
              timestamp: 31,
              b: 'b'
            }
          }
        },
        { a: 'a',
          timestamp: 66,
          b: {
            c: {
              d: {
                timestamp: 44,
                e: 23
              },
              f: 78
            },
            g: {
              h: 'rtz',
              timestamp: 55
            },
            j: {
              timestamp: 66
            }
          }
        }
      ]
      const traverseData = utils.traverse(testData, 10)
      expect(traverseData).to.be.deep.equal(exceptedData)
    })
  })
})
